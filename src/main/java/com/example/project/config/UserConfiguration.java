package com.example.project.config;

import com.example.project.repository.UserRepository;
import com.example.project.service.UserService;
import com.example.project.service.UserServiceImpl;
import com.example.project.service.UserServiceImpl2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration

public class UserConfiguration {

    @Value("${spring.profiles.active}")
    String profile;


    @Bean
    public UserService getUserServiceBean(UserRepository userRepo){

        return profile.equals("dev") ?  new UserServiceImpl(userRepo) : new UserServiceImpl2();
    }

}
