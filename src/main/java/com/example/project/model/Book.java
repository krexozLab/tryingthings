package com.example.project.model;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "books")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", unique = true)
    private String title;

    @Column(name = "author")
    private String author;

    @Column(name = "pages")
    private Integer pages;

    @Column(name = "publishing_house")
    private String publishingHouse;

    public Book(String title, String author, Integer pages, String publishingHouse) {
        this.title = title;
        this.author = author;
        this.pages = pages;
        this.publishingHouse = publishingHouse;
    }
}
