package com.example.project;

import com.example.project.model.Book;
import com.example.project.model.User;
import com.example.project.repository.BookRepository;
import com.example.project.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PreloadUsers {

    Logger log = LoggerFactory.getLogger(PreloadUsers.class);

    @Autowired
    UserRepository userRepository;

    public PreloadUsers(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Bean(name = "preloadingUsers")
    public CommandLineRunner commandLineRunner(UserRepository repository) {
        return (args -> {
            repository.save(new User("Gabi", "parola123"));
            repository.save(new User("Liviu", "parola456"));
            repository.save(new User("User", "password"));

            log.info("Preload Users: Gabi, Liviu, User");
        });
    }
}
