package com.example.project.service;

import com.example.project.api.response.UserDto;
import com.example.project.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    User loginUser(String username, String password);

    User saveUser(User user);

    List<User> listOfUsers();
}
