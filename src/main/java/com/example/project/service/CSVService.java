package com.example.project.service;

import com.example.project.helper.CSVHelper;
import com.example.project.model.Book;
import com.example.project.repository.BookRepository;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class CSVService {

    @Autowired
    BookRepository bookRepository;

    public void save(MultipartFile file) {
        try {
            List<Book> books = CSVHelper.csvToBooks(file.getInputStream());
            bookRepository.saveAll(books);
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }
    @SneakyThrows
    public ByteArrayInputStream load() {
        List<Book> books = bookRepository.findAll();
        ByteArrayInputStream in = CSVHelper.booksToCSV(books);
        return in;
    }

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }
}
