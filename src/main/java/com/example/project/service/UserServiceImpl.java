package com.example.project.service;

import com.example.project.api.response.UserDto;
import com.example.project.model.User;
import com.example.project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;

public class UserServiceImpl implements UserService, Serializable {

    @Autowired
    UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User loginUser(String username, String password){
        return repository.findByUsernameAndPassword(username, password);
    }

    @Override
    public User saveUser(User user) {
        User userSaved = new User(
                user.getUsername(),
                user.getPassword()
        );
        return repository.save(userSaved);
    }

    @Override
    public List<User> listOfUsers() {
        return repository.findAll();
    }

}
