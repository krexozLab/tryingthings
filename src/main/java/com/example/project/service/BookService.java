package com.example.project.service;

import com.example.project.model.Book;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BookService {

    Book saveBook(Book book);

    Book findBookByTitle(String title);

    List<Book> listOfBooks();

}
