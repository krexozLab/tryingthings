package com.example.project.api;

import com.example.project.api.request.BookDtoR;
import com.example.project.api.response.BookDto;
import com.example.project.model.Book;
import com.example.project.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/home")
public class BooksController {

    @Autowired
    BookService bookService;

    @GetMapping("/books")
    private List<Book> showAllBooks(){
        return bookService.listOfBooks();
    }

    @PostMapping("/books/title")
    private Book findBookByTitle(@RequestBody BookDtoR book){
        return bookService.findBookByTitle(book.getTitle());
    }

    @PostMapping("/books/add")
    private Book uploadBook(@RequestBody Book book){
        return bookService.saveBook(book);
    }
}
