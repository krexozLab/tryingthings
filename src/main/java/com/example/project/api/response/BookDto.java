package com.example.project.api.response;

import lombok.Data;


@Data
public class BookDto {

    private String title;

    private String author;
}
