package com.example.project.api.response;

import lombok.Data;

@Data
public class UserDto {

    private String username;
}
