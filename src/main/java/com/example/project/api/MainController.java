package com.example.project.api;

import com.example.project.api.request.UserDtoR;
import com.example.project.model.User;
import com.example.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/home")
public class MainController {

    @Autowired
    UserService userService;

    @PostMapping("/login")
    public Boolean loginUser(@RequestBody UserDtoR user) {
        return  userService.loginUser(
                user.getUsername(),
                user.getPassword()) != null;
    }

    @PostMapping("/register")
    public Boolean register(@RequestBody User user){
        return userService.saveUser(user) != null;
    }

    @GetMapping("/users")
    public List<User> listOfUsers(){
        return userService.listOfUsers();
    }

}