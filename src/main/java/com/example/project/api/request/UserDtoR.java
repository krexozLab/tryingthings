package com.example.project.api.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UserDtoR {

    @NotEmpty
    private String username;
    @NotEmpty
    private String password;
}