package com.example.project.api.request;

import lombok.Data;

@Data
public class BookDtoR {

    private String title;

    private String author;

}
