package com.example.project;

import com.example.project.model.Book;
import com.example.project.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PreloadBooks {

    Logger log = LoggerFactory.getLogger(PreloadUsers.class);

    @Autowired
    BookRepository bookRepository;

    public PreloadBooks(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    @Bean(name = "preloadingBooks")
    public CommandLineRunner commandLineRunner(BookRepository repository){
        return (args -> {
            repository.save(new Book("Book of Tury",
                    "Turiac Liviu",
                    279,
                    "Teleorman INC."));
            repository.save(new Book("Java Ultimate Edition",
                    "John Wick",
                    402,
                    "Oracle"));

            log.info("Preload Books: Book of Tury, Java Ultimate Edition");
        });
    }
}
