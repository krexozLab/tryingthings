# TryingThings

## Welcome to TryingThings 1.0

A simple Spring Boot application.
>
>>Using the app:
> - To upload CSV files
> - To download CSV files with Data from DB
> - To access the users/books in DB
> - To login/register users via POST @RequestBody
> - To insert data in DB via CSV file with Postman
> - Using PostgreSQL
> - Optimising code with Lombok
>
>>MAPPING: localhost:8090/home
>- GET : /users & /books & /books/show
>- POST : /books/title & /books/add & /login & /register
>- Upload via Postman CSV : /upload/book (CSV Only)
>- Donwloading CSV file with all Books from DB
> with GET method at : /books/download
>>Dependencies:
>- JPA
>- Thymeleaf
>- Web Services
>- Commons CSV
>- Lombok
>- PostgreSQL
